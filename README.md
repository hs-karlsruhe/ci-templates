# Linting

![pipeline status](https://gitlab.com/hs-karlsruhe/ci-templates/badges/master/pipeline.svg)
[![coverage](https://gitlab.com/hs-karlsruhe/ci-templates/badges/master/coverage.svg)](https://hs-karlsruhe.gitlab.io/ci-templates/test/)
[![pylint](https://hs-karlsruhe.gitlab.io/ci-templates/badges/pylint.svg)](https://hs-karlsruhe.gitlab.io/ci-templates/lint/)
![markdownlint](https://hs-karlsruhe.gitlab.io/ci-templates/badges/markdownlint.svg)
![yamllint](https://hs-karlsruhe.gitlab.io/ci-templates/badges/yamllint.svg)

Calculation basis for quality indices is the file content but with ignoring empty lines.

## Usage

You can use the project templates by including them in your ci file.

```yaml
include:
  - project: hs-karlsruhe/ci-templates
    ref: master
    file: lint.gitlab-ci.yml
  - project: hs-karlsruhe/ci-templates
    ref: master
    file: python.gitlab-ci.yml
  - project: hs-karlsruhe/ci-templates
    ref: master
    file: pages.gitlab-ci.yml

stages:
  - init
  - test
  - build
  - deploy

variables:
  # example: PYTHON_VERSION: "3.8"
  PYTHON_VERSION: "<YOUR-PYTHON-VERSION>"
```

Once you include `pages.gitlab-ci.yml` you can use following badges after replacing the placeholders `SUB_PATH` with the GitLab project path of your project (e.g. `SUB_PATH` of `https://gitlab.com/hs-karlsruhe/ws2021-22/must0004/projektarbeit` is `ws2021-22/must0004/projektarbeit`):

-   Coverage

    ```Markdown
    [![coverage](https://gitlab.com/hs-karlsruhe/SUB_PATH/badges/master/coverage.svg)](https://hs-karlsruhe.gitlab.io/SUB_PATH/test/)
    ```

-   Pylint

    ```Markdown
    [![pylint](https://hs-karlsruhe.gitlab.io/SUB_PATH/badges/pylint.svg)](https://hs-karlsruhe.gitlab.io/SUB_PATH/lint/)
    ```

-   Markdownlint

    ```Markdown
    ![markdownlint](https://hs-karlsruhe.gitlab.io/SUB_PATH/badges/markdownlint.svg)
    ```

-   Yamllint

    ```Markdown
    ![yamllint](https://hs-karlsruhe.gitlab.io/SUB_PATH/badges/yamllint.svg)
    ```


## Quality index

### Markdown

```math
10 - (warnings / lines) * 40
```

### yaml

```math
10 - ((10 * errors + 4 * warnings) / lines) * 10
```

### pylint

```math
10.0 - ((float(5 * error + warning + refactor + convention) / statement) * 10)
```
